/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  27/04/2019 14:25:05                      */
/*==============================================================*/


drop table if exists CATEGORIE;

drop table if exists CLIENT;


drop table if exists IMAGE;


drop table if exists PRODUIT;

/*==============================================================*/
/* Table : CATEGORIE                                            */
/*==============================================================*/
create table CATEGORIE
(
   IDCA                 bigint not null AUTO_INCREMENT,
   NOM                  varchar(50),
   IDMERE bigint ,
   IMAGE 				varchar(50),
   primary key (IDCA),
   foreign key(IDMERE) references categorie(idca) 
);

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT
(
   IDCL                 bigint not null AUTO_INCREMENT,
   NOM                  varchar(50),
   MDP                  varchar(100),
   primary key (IDCL)
);

/*==============================================================*/
/* Table : DETAILPANIER                                         */
/*==============================================================*/


/*==============================================================*/
/* Table : IMAGE                                                */
/*==============================================================*/
create table IMAGE
(
   IDIM                 bigint not null AUTO_INCREMENT,
   IDPR                 bigint,
   NOM                  varchar(50),
   primary key (IDIM)
);

/*==============================================================*/
/* Table : PANIER                                               */
/*==============================================================*/


/*==============================================================*/
/* Table : PRODUIT                                              */
/*==============================================================*/
create table PRODUIT
(
   IDPR                 bigint not null AUTO_INCREMENT,
   IDCA                 bigint not null,
   NOM                  varchar(50),
   PRIX                 decimal(18,2),
   DESCRIPTION          text,
   primary key (IDPR)
);


alter table IMAGE add constraint FK_PRODUITIMAGE foreign key (IDPR)
      references PRODUIT (IDPR) on delete restrict on update restrict;

  references CLIENT (IDCL) on delete restrict on update restrict;

alter table PRODUIT add constraint FK_CATEGORIEPRODUIT foreign key (IDCA)
      references CATEGORIE (IDCA) on delete restrict on update restrict;

